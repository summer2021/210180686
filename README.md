# rt-boot

## 1、介绍
基于RT-Thread上，可以引导其他操作系统，包括Linux的bootloader

### 1.1 目录结构
| 名称 | 说明 |
| ---- | ---- |
| docs  | 文档目录 |
| inc  | 头文件目录 |
| src  | 源代码目录 |

### 1.2 许可证
Apache 2.0 LICENSE

### 1.3 依赖
- RT-Thread 3.0+

## 2、如何打开 rt-boot
使用 rt-boot package 需要在 RT-Thread 的包管理器中选择它，具体路径如下：

```
RT-Thread online packages
    tools packages --->
        [*] RT-Boot package
```

## 3、使用 rt-boot
在打开 rt-boot package 后，当进行 bsp 编译时，它会被加入到 bsp 工程中进行编译。

* 完整的 API 手册可以访问这个[链接](docs/api.md)
* 更多文档位于 [`/docs`](/docs) 下，使用前 **务必查看**

## 4、注意事项
utils中均是对第三方库进行封装
如果发生`libfdt`库冲突，可选择将`libfdt`不参与构建。

## 5、联系方式

* 维护：GuEe-GUI
* 主页：https://gitlab.summer-ospp.ac.cn/summer2021/210180686