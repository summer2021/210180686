# rt-boot linux引导示例

##### 提示：当前内核、设备树、initrd加载方式均依赖文件系统。

## ARM Linux
如果使用 ATAG 设备描述方式，用户需要自行填入以下数据：
```c
/* ATAG：设备ID，在`arm_mach_types.h'中定义 */
#define BSP_MACHINE_ID
/* ATAG：设备修订编号 */
#define BSP_MACHINE_REV
/* ATAG：串口编号 */
#define BSP_SERIAL_NR
/* ATAG：Linux可用内存大小 */
#define BSP_MACHINE_MEM
```
以qemu-vexpress-a9平台为例，该平台需要设置 1024MB 内存。

### 示例结果
执行 `rt_boot auto` 后，Linux能正确进入shell界面则说明启动成功。

## AArch64 Linux

### qemu-virt64-aarch64
使用QEMU dump出virtio设备树，该平台需要设置 1024~4096MB 内存：
```sh
qemu-system-aarch64 -M virt,gic-version=2 dumpdtb=virt-aarch64.dtb -cpu cortex-a53 -smp 4 -m 4096
```

### raspi3-64 qemu
使用QEMU dump出virtio设备树：
```sh
qemu-system-aarch64 -M raspi3,dumpdtb=raspi3b.dtb -cpu cortex-a53 -smp 4 -kernel kernel8.img -dtb bcm2710-rpi-3-b.dtb
```

### raspi4-64：
在替换树莓派bootloader之前，在linux内dump出设备树：
```sh
cp /sys/firmware/fdt raspi4b.dtb
```
该测试用例使用`openwrt-21.02.0-rc4-bcm27xx-bcm2711-rpi-4-ext4-factory`，并且已安装好rootfs。

### 示例结果 ####
执行 `rt_boot auto` 后，Linux能正确进入shell界面则说明启动成功。
