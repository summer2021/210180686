# rt-boot 辅助工具示例 #

#### 基于ymodem串口传输协议文件传输 ####
```c
void rt_boot_cmd_ymodem(char **argv);
```
menuconfig配置：
```
RT-Thread Components
    Utilities  --->
        [*] Enable Ymodem
        [*]   Enable file transfer feature
```
命令示例，可以通过MobaXterm发送文件
```sh
# ymodem <save-filename>
rt_boot ymodem rtthread.bin
```

#### 示例结果 ####
```sh
msh />rt_boot ymodem rtthread.bin
Connecting...
CC

Get file success, size: 606416 bytes.
msh />
```

#### 基于http网络传输协议文件传输（默认端口：80） ####
```c
void rt_boot_cmd_http(char **argv)
```
menuconfig配置：
```
RT-Thread Components
    Network  --->
        Socket abstraction layer  --->
            [*] Enable socket abstraction layer
        Network interface device  --->
            [*] Enable network interface device
        light weight TCP/IP stack  --->
            (2048) the stack size of lwIP thread
            (2048) the stack size of ethernet thread
```
命令示例
```sh
# http <http-host|http-host:port> <http-filename> <save-filename>
rt_boot http 192.168.12.73 kernel8.img kernel8.img
```

#### 示例结果 ####
```sh
msh />rt_boot http 192.168.12.73 kernel8.img kernel8.img
Connecting to 192.168.12.73:80...
Sending http request...
Receiving...
9MB
Get file end, size: 9486344 bytes.
msh />
```

#### 基于tftp网络传输协议文件传输（默认端口：69） ####
```c
void rt_boot_cmd_tftp(char **argv)
```
menuconfig配置：
```
RT-Thread Components
    Network  --->
        Socket abstraction layer  --->
            [*] Enable socket abstraction layer
        Network interface device  --->
            [*] Enable network interface device
        light weight TCP/IP stack  --->
            (2048) the stack size of lwIP thread
            (2048) the stack size of ethernet thread
```
命令示例
```sh
# tftp <tftp-host|tftp-host:port> <tftp-filename> <save-filename>
rt_boot tftp 192.168.12.73:69 initrd.img initrd.img
```

#### 示例结果 ####
```sh
msh />rt_boot tftp 192.168.12.73:69 initrd.img initrd.img
Sending read request...
Receiving...
10MB
Get file end, size: 10515957 bytes.
msh />
```