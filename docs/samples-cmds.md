# rt-boot 辅助工具示例 #

#### 命令格式
##### 为了防止和内核功能函数冲突，命令方式均为：
```c
RT_BOOT_CMD(xxx, "Xxxx", char **argv)
{
    /* codes... */
}
```

#### 执行多个命令
在`rt-boot/src/config.h`中有`rt_boot_auto_cmds`定义，可以添加命令。参数间空格隔开，命令间分号隔开，带空格的参数需要单引号包围：
```c
const char *rt_boot_auto_cmds =
#ifdef SOC_XXX
    "linux xxx 'xxx';"
    "dtb xxx.dtb;"
    "initrd initrd.img;"
    "boot"
#endif
```
控制台执行`rt_boot auto`后，rt_boot就会自动解析并执行多个命令。
