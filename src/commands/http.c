/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rtthread.h>

#include <rt-boot/cmd.h>

#ifdef RT_USING_LWIP
#include <lwip/sockets.h>
#include <lwip/netdb.h>

#include <unistd.h>
#include <fcntl.h>

#define HTTP_END     "\r\n\r\n"
#define HTTP_END_SZ  (sizeof(HTTP_END) / sizeof(char) - 1)
#define HTTP_REQUEST "GET /%s HTTP/1.1\r\nHost: %s" HTTP_END

#define RECV_BUF_SZ  1024
#define DEFAULT_PORT 80
#endif /* RT_USING_LWIP */

RT_BOOT_CMD(http, "Get file from http server", char **argv)
{
#ifdef RT_USING_LWIP
    int sock = -1;
    char host[sizeof("255.255.255.255") / sizeof(char) - 1];
    int port = DEFAULT_PORT;
    struct sockaddr_in server_addr;
    struct hostent *get_hostent;

    char *send_buf = RT_NULL, *recv_buf = RT_NULL;
    int data_sz, data_total_sz = 0;
    int MB_sz = -1;
    int file_fd = -1;

    if (argv[0] == RT_NULL || argv[1] == RT_NULL || argv[2] == RT_NULL)
    {
        rt_boot_cmd_help("http <host[:port]> <http-filename> <save-filename>\n");
        goto end;
    }
    else
    {
        int i = 0;
        char *addr = argv[0];
        while (*addr)
        {
            if (*addr != ':')
            {
                host[i++] = *addr;
            }
            else
            {
                host[i] = '\0';
                if (*++addr != '\0')
                {
                    port = atoi(addr);
                }
                break;
            }
            ++addr;
        }
    }

    file_fd = open(argv[2], O_CREAT | O_RDWR | O_TRUNC, 0);

    if (file_fd == -1)
    {
        rt_kprintf("File name `%s' is invalid.\n", argv[2]);
        return;
    }

    if ((get_hostent = lwip_gethostbyname(host)) == RT_NULL)
    {
        rt_kprintf("Host name `%s' error.\n", host);
        /* get_hostent no need to free */
        goto end;
    }

    if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        rt_kprintf("Init socket fail.\n");
        goto end;
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = lwip_htons(port);
    server_addr.sin_addr = *((struct in_addr *)get_hostent->h_addr);
    rt_memset(&(server_addr.sin_zero), 0, 8);

    rt_kprintf("Connecting to %s:%d...\n", host, port);
    if (lwip_connect(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
    {
        rt_kprintf("Connect fail.\n");
        goto end;
    }

    data_sz = sizeof(char) * sizeof(HTTP_REQUEST) + rt_strlen(argv[1]) + rt_strlen(host);
    send_buf = (char *)rt_malloc(data_sz);
    recv_buf = (char *)rt_malloc(sizeof(char) * RECV_BUF_SZ);
    if (send_buf == RT_NULL || recv_buf == RT_NULL)
    {
        rt_kprintf("\nMemory(%d bytes) is not enough.\n", data_sz);
        goto end;
    }
    rt_snprintf(send_buf, data_sz, HTTP_REQUEST, argv[1], host);

    rt_kprintf("Sending http request...\n");
    if (lwip_send(sock, send_buf, data_sz, 0) == -1)
    {
        rt_kprintf("Sending fail.\n");
        goto end;
    }

    rt_kprintf("Receiving...\n");
    while ((data_sz = lwip_recv(sock, recv_buf, RECV_BUF_SZ, MSG_WAITALL)) > 0)
    {
/* disable lwip_write macros */
#undef write
        if (MB_sz > -1)
        {
            data_total_sz += data_sz;
            if (data_total_sz >> 20 > MB_sz)
            {
                ++MB_sz;
                rt_kprintf("\r%dMB", MB_sz);
            }
            write(file_fd, recv_buf, data_sz);
        }
        else
        {
           char *data_start = rt_strstr(recv_buf, HTTP_END);
           if (data_start != RT_NULL && (data_sz -= (data_start - recv_buf + HTTP_END_SZ)) >= 0)
           {
               write(file_fd, data_start + HTTP_END_SZ, data_sz);
               MB_sz = 0;
               rt_kprintf("\r%dMB", MB_sz);
           }
        }
    }

    rt_kprintf("\nGet file end, size: %d bytes.\n", data_total_sz);

end:
    if (sock != -1)
    {
        lwip_close(sock);
    }
/* disable lwip_close macros */
#undef close
    if (file_fd != -1)
    {
        close(file_fd);
    }
    if (send_buf != RT_NULL)
    {
        rt_free(send_buf);
    }
    if (recv_buf != RT_NULL)
    {
        rt_free(recv_buf);
    }
#else
    rt_kprintf("No configured `RT_USING_LWIP'.\n");
#endif /* RT_USING_LWIP */
}