/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rthw.h>
#include <rtthread.h>

#include <mmu.h>
#include <psci.h>
#include <gtimer.h>
#include <interrupt.h>

#include <board.h>

#include <rt-boot/cmd.h>
#include <rt-boot/macros.h>
#include <rt-boot/kernel.h>
#include <rt-boot/utils/fdt.h>
#include <rt-boot/utils/file.h>
#include <rt-boot/utils/gzip.h>

#define LINUX_AARCH64_IMAGE_MAGIC 0x644d5241

struct kernel_image_head
{
    rt_uint32_t code0;          /* executable code */
    rt_uint32_t code1;          /* executable code */
    rt_uint64_t text_offset;    /* image load offset from start of RAM, little endian */
    rt_uint64_t image_size;     /* effective Image size, little endian */
    rt_uint64_t flags;          /* kernel flags, little endian */
    rt_uint64_t res2;           /* reserved */
    rt_uint64_t res3;           /* reserved */
    rt_uint64_t res4;           /* reserved */
    rt_uint32_t magic;          /* magic number, little endian, "ARM\x64" */
    rt_uint32_t res5;           /* reserved (used for PE COFF offset) */
};

#define LINUX_LOAD_ADDR     kernel_ram_start
#define LINUX_INITRD_ADDR   (kernel_ram_start + 0x3000000)
#define LINUX_DTB_ADDR      (LINUX_INITRD_ADDR - (64 * KB))

static char *kernel_cmdline = RT_NULL;
static rt_ubase_t kernel_ram_start;
static rt_ubase_t kernel_ram_start_backup;
#ifdef RT_USING_SMP
static rt_ubase_t kernel_cpu_release_addr;
#endif
static rt_size_t linux_size = 0;
static rt_size_t initrd_size = 0;
static rt_size_t dtb_size = 0;

static int _kernel_ram_init()
{
    rt_ubase_t kernel_ram_fixed;

#ifdef HEAP_END
    kernel_ram_start = (rt_ubase_t)HEAP_END;
#endif
#ifdef RT_HW_HEAP_END
    kernel_ram_start = (rt_ubase_t)RT_HW_HEAP_END;
#endif

#ifdef RT_USING_SMP
    kernel_cpu_release_addr = kernel_ram_start;
    *(rt_ubase_t *)kernel_cpu_release_addr = 0;
    rt_hw_cpu_dcache_ops(RT_HW_CACHE_FLUSH, (rt_ubase_t *)kernel_cpu_release_addr, sizeof(kernel_cpu_release_addr));
    rt_hw_cpu_dcache_ops(RT_HW_CACHE_FLUSH, (rt_ubase_t *)&kernel_cpu_release_addr, sizeof(kernel_cpu_release_addr));
#endif

    /* 2MB memory aligned */
    kernel_ram_fixed = kernel_ram_start & (~0x1fffff);

    if (kernel_ram_fixed < kernel_ram_start)
    {
        kernel_ram_fixed += 2 * MB;
    }

    kernel_ram_start = kernel_ram_fixed;
    kernel_ram_start_backup = kernel_ram_start;

    /* 64MB is enough */
    rt_hw_mmu_map(kernel_ram_start_backup, 64 * MB, NORMAL_MEM);

    return 0;
}
INIT_APP_EXPORT(_kernel_ram_init);

static int _decompress_kernel()
{
    int result;
    gzip_metadata_t metadata;
    rt_uint8_t *inflated, *gz_ptr = (rt_uint8_t *)LINUX_LOAD_ADDR;

    result = gzip_get_metadata(gz_ptr, linux_size, &metadata);

    if (result == 1)
    {
        /* not a gzip file */
        return 0;
    }
    else if (result == -1)
    {
        rt_kprintf("Unsupported compression method");

        return -1;
    }

    /* 2MB memory aligned */
    kernel_ram_start += linux_size;
    kernel_ram_start &= ~0x1fffff;
    if (kernel_ram_start < (rt_uint64_t)gz_ptr + linux_size)
    {
        kernel_ram_start += 2 * MB;
    }

    /* LINUX_LOAD_ADDR was updated */
    inflated = (rt_uint8_t *)LINUX_LOAD_ADDR;
    gzip_inflate(&gz_ptr[metadata.block_offset], inflated);
    gzip_abandon_metadata(&metadata);

    if (gzip_crc32(inflated, metadata.footer.isize) != metadata.footer.crc32)
    {
        linux_size = 0;
        kernel_ram_start = kernel_ram_start_backup;

        rt_kprintf("File gzip crc32 check fail");

        return -1;
    }

    linux_size = metadata.footer.isize;

    return 0;
}

RT_BOOT_CMD(linux, "Load linux kernel", char **argv)
{
    kernel_ram_start = kernel_ram_start_backup;
    kernel_set_type(KERNEL_TYPE_EMPTY);

    switch (file_load_to_memory((void *)LINUX_LOAD_ADDR, argv[0], &linux_size, RT_NULL))
    {
    case FILE_LOAD_OK:
        /* save the cmdline */
        if (argv[1] != RT_NULL)
        {
            int cmdline_len = rt_strlen(argv[1]);

            if (cmdline_len > 0)
            {
                if (kernel_cmdline != RT_NULL)
                {
                    rt_free(kernel_cmdline);
                }

                kernel_cmdline = (char *)rt_malloc(sizeof(char) * (cmdline_len + 1));

                if (kernel_cmdline != RT_NULL)
                {
                    rt_memcpy(kernel_cmdline, argv[1], cmdline_len + 1);
                }
                else
                {
                    linux_size = 0;
                    rt_kprintf("No memory to save cmdline.");
                    break;
                }
            }
        }

        /* check kernel and decompress kernel image if compress (gzip only) */
        if (!_decompress_kernel())
        {
            struct kernel_image_head *arm64_linux_head = (struct kernel_image_head *)LINUX_LOAD_ADDR;

            if (arm64_linux_head->magic == LINUX_AARCH64_IMAGE_MAGIC)
            {
                rt_uint64_t text_offset = 0;

                if (arm64_linux_head->image_size == 0)
                {
                    text_offset = 0x80000;
                }

                if (arm64_linux_head->text_offset != 0)
                {
                    text_offset = arm64_linux_head->text_offset;
                    rt_kprintf("Moving Linux image to address 0x%x...\n", LINUX_LOAD_ADDR + text_offset);
                    rt_memmove((void *)(LINUX_LOAD_ADDR + text_offset), (void *)LINUX_LOAD_ADDR, linux_size);
                }

                /* LINUX_LOAD_ADDR update */
                kernel_ram_start += text_offset;
                rt_kprintf("Kernel size: %d bytes, at address 0x%x ~ 0x%x.\n",
                        linux_size, LINUX_LOAD_ADDR, LINUX_LOAD_ADDR + linux_size);

                kernel_set_type(KERNEL_TYPE_LINUX);
            }
            else
            {
                linux_size = 0;
                rt_kprintf("Not an ARM64 Linux.\n");
                break;
            }
        }
        else
        {
            rt_kprintf(", decompress Linux fail.\n");
        }
    break;
    case FILE_NAME_EMPTY:
        rt_kprintf("Invalid kernel file.\n");
    break;
    case FILE_INVALID:
        rt_boot_cmd_help("linux <<kernel-path> <kernel-args>|<kernel-path>>\n");
    break;
    default: break;
    }
}

static int _dtb_check_cb(int fd)
{
    unsigned char file_first_int[4];

    lseek(fd, 0, SEEK_SET);
    read(fd, (void*)file_first_int, sizeof(rt_uint32_t));

    if (fdt_magic(file_first_int) != FDT_MAGIC)
    {
        rt_kprintf("dtb is invalid.\n");
        return -1;
    }

    return 0;
}

RT_BOOT_CMD(dtb, "Load devicetree", char **argv)
{
    if (linux_size == 0)
    {
        kernel_put_no_load();
        return;
    }

    switch (file_load_to_memory((void *)LINUX_DTB_ADDR, argv[0], &dtb_size, &_dtb_check_cb))
    {
    case FILE_LOAD_OK:
    #ifdef RT_USING_SMP
        dtb_size = fdt_install_spin_table((void *)LINUX_DTB_ADDR, kernel_cpu_release_addr);
    #endif
        if (kernel_cmdline != RT_NULL)
        {
            dtb_size = fdt_install_cmdline((void *)LINUX_DTB_ADDR, kernel_cmdline, rt_strlen(kernel_cmdline) + 1);
            rt_free(kernel_cmdline);
        }
        rt_kprintf("Device tree size: %d bytes, at address 0x%x ~ 0x%x.\n",
                dtb_size, LINUX_DTB_ADDR, LINUX_DTB_ADDR + dtb_size);
    break;
    case FILE_NAME_EMPTY:
        rt_boot_cmd_help("dtb <dtb-path>\n");
    break;
    case FILE_INVALID:
        rt_kprintf("Invalid device tree file.\n");
    break;
    default: break;
    }
}

RT_BOOT_CMD(initrd, "Load initrd", char **argv)
{
    if (linux_size == 0)
    {
        kernel_put_no_load();
        return;
    }

    switch (file_load_to_memory((void *)LINUX_INITRD_ADDR, argv[0], &initrd_size, RT_NULL))
    {
    case FILE_LOAD_OK:
        if (dtb_size > 0)
        {
            dtb_size = fdt_install_initrd((void *)LINUX_DTB_ADDR, LINUX_INITRD_ADDR, initrd_size);
        }
        rt_kprintf("Initrd size: %d bytes, at address 0x%x ~ 0x%x.\n",
                initrd_size, LINUX_INITRD_ADDR, LINUX_INITRD_ADDR + initrd_size);
    break;
    case FILE_NAME_EMPTY:
        rt_boot_cmd_help("initrd <initrd-path>\n");
    break;
    case FILE_INVALID:
        rt_kprintf("Invalid initrd file.\n");
    break;
    default: break;
    }
}

static void _cpu_prep_linux()
{
    volatile rt_ubase_t reg_val = 0;

#ifdef RT_USING_SMP
    rt_hw_local_irq_disable();
#else
    rt_hw_interrupt_disable();
#endif

    rt_hw_gtimer_disable();

    /* masked Debug, SError, IRQ and FIQ */
    reg_val = 0x3c9;
    __asm__ volatile ("msr spsr_el1, %0"::"r"(reg_val));

    rt_hw_dcache_flush_all();

    __asm__ volatile ("mrs %0, sctlr_el1":"=r"(reg_val));
    /* MMU = off */
    reg_val &= ~1;
    /* D-cache = off */
    reg_val &= ~(1 << 2);
    /* I-cache = dont care and clean PoC */
    __asm__ volatile (
        "msr    sctlr_el1, %0\n\r"
        "isb    sy\n\r"
        "tlbi   vmalle1is\n\r"
        "dsb    sy\n\r"
        "isb    sy"
        ::"r"(reg_val));

    /* enable FP/SIMD */
    reg_val = 3 << 20;
    __asm__ volatile ("msr cpacr_el1, %0"::"r"(reg_val));
}

static void __attribute__ ((noreturn)) jump_to_linux()
{
    int i;
    void (*kernel_entry)();

    _cpu_prep_linux();

    /* mask all ppi and spi */
    for (i = 16; i < MAX_HANDLERS; ++i)
    {
        rt_hw_interrupt_mask(i);
    }

    kernel_put_jump_start();

    /*
     *  x0 = dtb ptr
     *  x1 = 0 (reserved for future use)
     *  x2 = 0 (reserved for future use)
     *  x3 = 0 (reserved for future use)
     */
    kernel_entry = (void(*)(rt_uint64_t x0, rt_uint64_t x1, rt_uint64_t x2, rt_uint64_t x3))LINUX_LOAD_ADDR;

    /* call kernel entry, stext */
    kernel_entry(LINUX_DTB_ADDR, 0, 0, 0, 0);

    __builtin_unreachable();
}

#ifdef RT_USING_SMP
static void __attribute__ ((noreturn)) _smp_cpu_init(int irqno, void *param)
{
    /* irqno may not really org irq of interrupt controller */
    rt_hw_interrupt_ack(rt_hw_interrupt_get_irq());

    _cpu_prep_linux();

    if (param != RT_NULL)
    {
        if (!rt_strcmp(param, "psci"))
        {
            /* off this cpu that linux could restart it */
            arm_psci_cpu_off(PSCI_POWER_STATE(0, 0, PSCI_POWER_STATE_ID(0, 0, 0, 3)));
        }
    }

    /*
     *  x0 = 0 (reserved for future use)
     *  x1 = 0 (reserved for future use)
     *  x2 = 0 (reserved for future use)
     *  x3 = 0 (reserved for future use)
     */
    __asm__ volatile (
        "   mov x4, %0\n\r"
        "   mov x0, #0\n\r"
        "   mov x1, #0\n\r"
        "   mov x2, #0\n\r"
        "   mov x3, #0\n\r"
        "1: wfe\n\r"
        "   ldr x5, [x4]\n\r"
        "   cmp x5, #0\n\r"
        "   beq 1b\n\r"
        "   br  x5\n\r"
        ::"r"(kernel_cpu_release_addr));

    __builtin_unreachable();
}
#endif /* RT_USING_SMP */

void kernel_linux_boot()
{
#ifdef RT_USING_SMP
    char *smp_param = RT_NULL;
#endif

    if (dtb_size <= 0)
    {
        rt_kprintf("Device tree not load.\n");
        return;
    }

#ifdef RT_USING_SMP
    if (fdt_path_offset((void *)LINUX_DTB_ADDR, "/psci") >= 0)
    {
        smp_param = "psci";
    }

    rt_hw_interrupt_install(RT_BOOT_CPU_SYNC_IPI, _smp_cpu_init, smp_param, "cpu_sync");

    rt_hw_ipi_send(RT_BOOT_CPU_SYNC_IPI, RT_CPU_MASK ^ (1 << rt_hw_cpu_id()));
#endif

    jump_to_linux();
}
