/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rthw.h>
#include <rtthread.h>

#include <cp15.h>
#include <interrupt.h>

#include <board.h>

#include <rt-boot/cmd.h>
#include <rt-boot/kernel.h>
#include <rt-boot/macros.h>
#include <rt-boot/utils/atag.h>
#include <rt-boot/utils/fdt.h>
#include <rt-boot/utils/file.h>

#include "arm_mach_type.h"

#ifndef BSP_MACHINE_ID
#define BSP_MACHINE_ID  0xffffffff
#endif
#ifndef BSP_MACHINE_REV
#define BSP_MACHINE_REV 0x1
#endif
#ifndef BSP_SERIAL_NR
#define BSP_SERIAL_NR   ((rt_uint64_t)0)
#endif
#ifndef BSP_MACHINE_MEM
#define BSP_MACHINE_MEM (1024 * MB)
#endif

#define LINUX_KERNEL_ADDR   (kernel_ram_start + 0x8000)
#define LINUX_INITRD_ADDR   (kernel_ram_start + 0x3000000)
#define LINUX_DTB_ADDR      (LINUX_INITRD_ADDR - (64 * KB))

static char *kernel_cmdline = RT_NULL;
static struct tag *kernel_params;
static rt_ubase_t kernel_ram_start;
#ifdef RT_USING_SMP
static rt_ubase_t kernel_cpu_release_addr;
#endif
static rt_size_t linux_size = 0;
static rt_size_t initrd_size = 0;
static rt_size_t dtb_size = 0;

static int _kernel_ram_init()
{
#ifdef HEAP_END
    kernel_ram_start = (rt_ubase_t)HEAP_END;
#endif
#ifdef RT_HW_HEAP_END
    kernel_ram_start = (rt_ubase_t)RT_HW_HEAP_END;
#endif

#ifdef RT_USING_SMP
    kernel_cpu_release_addr = kernel_ram_start;
    *(rt_ubase_t *)kernel_cpu_release_addr = 0;
    rt_hw_cpu_dcache_ops(RT_HW_CACHE_FLUSH, (rt_ubase_t *)kernel_cpu_release_addr, sizeof(kernel_cpu_release_addr));
    rt_hw_cpu_dcache_ops(RT_HW_CACHE_FLUSH, (rt_ubase_t *)&kernel_cpu_release_addr, sizeof(kernel_cpu_release_addr));
#endif

    kernel_ram_start += 0x1000;
    kernel_ram_start &= RT_BOOT_MAX_ADDR ^ 0xfff;

    return 0;
}
INIT_APP_EXPORT(_kernel_ram_init);

static void _setup_tag_core()
{
    kernel_params->hdr.tag = ATAG_CORE;
    kernel_params->hdr.size = tag_size(tag_core);

    kernel_params->u.core.flags = 0;
    kernel_params->u.core.pagesize = 0;
    kernel_params->u.core.rootdev = 0;

    kernel_params = tag_next(kernel_params);
}

static void _setup_tag_initrd(rt_uint32_t start, rt_uint32_t size)
{
    kernel_params->hdr.tag = ATAG_INITRD2;
    kernel_params->hdr.size = tag_size(tag_initrd);

    kernel_params->u.initrd.start = start;
    kernel_params->u.initrd.size = size;

    kernel_params = tag_next(kernel_params);
}

static void _setup_tag_serialnr(rt_uint32_t high, rt_uint32_t low)
{
    kernel_params->hdr.tag = ATAG_SERIAL;
    kernel_params->hdr.size = tag_size(tag_serialnr);

    kernel_params->u.serialnr.high = high;
    kernel_params->u.serialnr.low = low;

    kernel_params = tag_next(kernel_params);
}

static void _setup_tag_revision(rt_uint32_t rev)
{
    kernel_params->hdr.tag = ATAG_REVISION;
    kernel_params->hdr.size = tag_size(tag_revision);

    kernel_params->u.revision.rev = rev;

    kernel_params = tag_next(kernel_params);
}

static void _setup_tag_mem32(rt_uint32_t start, rt_uint32_t size)
{
    kernel_params->hdr.tag = ATAG_MEM;
    kernel_params->hdr.size = tag_size(tag_mem32);

    kernel_params->u.mem.start = start;
    kernel_params->u.mem.size = size;

    kernel_params = tag_next(kernel_params);
}

static void _setup_tag_cmdline(char *cmdline)
{
    int cmdline_len = rt_strlen(kernel_cmdline) + 1;

    kernel_params->hdr.tag = ATAG_CMDLINE;
    kernel_params->hdr.size = (cmdline_len + 3 + sizeof(struct tag_header)) >> 2;

    rt_memcpy(kernel_params->u.cmdline.cmdline, kernel_cmdline, cmdline_len);
    rt_free(kernel_cmdline);

    kernel_params = tag_next(kernel_params);
}

static void _setup_tag_end()
{
    kernel_params->hdr.tag = ATAG_NONE;
    kernel_params->hdr.size = 0;
}

RT_BOOT_CMD(linux, "Load linux kernel", char **argv)
{
    kernel_set_type(KERNEL_TYPE_EMPTY);

    switch (file_load_to_memory((void *)LINUX_KERNEL_ADDR, argv[0], &linux_size, RT_NULL))
    {
    case FILE_LOAD_OK:
        /* save the cmdline */
        if (argv[1] != RT_NULL)
        {
            int cmdline_len = rt_strlen(argv[1]);

            if (cmdline_len > 0)
            {
                if (kernel_cmdline != RT_NULL)
                {
                    rt_free(kernel_cmdline);
                }

                kernel_cmdline = (char *)rt_malloc(sizeof(char) * (cmdline_len + 1));

                if (kernel_cmdline != RT_NULL)
                {
                    rt_memcpy(kernel_cmdline, argv[1], cmdline_len + 1);
                }
                else
                {
                    linux_size = 0;
                    rt_kprintf("No memory to save cmdline.");
                    break;
                }
            }
        }

        rt_kprintf("Kernel size: %d bytes, at address 0x%x ~ 0x%x.\n",
                linux_size, LINUX_KERNEL_ADDR, LINUX_KERNEL_ADDR + linux_size);

        kernel_set_type(KERNEL_TYPE_LINUX);
    break;
    case FILE_NAME_EMPTY:
        rt_boot_cmd_help("linux <path> [cmdline]\n");
    break;
    case FILE_INVALID:
        rt_kprintf("Invalid kernel file.\n");
    break;
    default: break;
    }
}

static int _dtb_check_cb(int fd)
{
    rt_uint8_t file_first_int[4];

    lseek(fd, 0, SEEK_SET);
    read(fd, (void*)file_first_int, sizeof(rt_uint32_t));

    if (fdt_magic(file_first_int) != FDT_MAGIC)
    {
        rt_kprintf("dtb is invalid.\n");
        return -1;
    }

    return 0;
}

RT_BOOT_CMD(dtb, "Load devicetree", char **argv)
{
    if (linux_size == 0)
    {
        kernel_put_no_load();
        return;
    }

    switch (file_load_to_memory((void *)LINUX_DTB_ADDR, argv[0], &dtb_size, &_dtb_check_cb))
    {
    case FILE_LOAD_OK:
    #ifdef RT_USING_SMP
        dtb_size = fdt_install_spin_table((void *)LINUX_DTB_ADDR, kernel_cpu_release_addr);
    #endif
        if (kernel_cmdline != RT_NULL)
        {
            dtb_size = fdt_install_cmdline((void *)LINUX_DTB_ADDR, kernel_cmdline, rt_strlen(kernel_cmdline) + 1);
            rt_free(kernel_cmdline);
        }
        rt_kprintf("Device tree size: %d bytes, at address 0x%x ~ 0x%x.\n",
                dtb_size, LINUX_DTB_ADDR, LINUX_DTB_ADDR + dtb_size);
    break;
    case FILE_NAME_EMPTY:
        rt_boot_cmd_help("dtb <dtb-path>\n");
    break;
    case FILE_INVALID:
        rt_kprintf("Invalid device tree file.\n");
    break;
    default: break;
    }
}

RT_BOOT_CMD(initrd, "Load initrd", char **argv)
{
    if (linux_size == 0)
    {
        kernel_put_no_load();
        return;
    }

    switch (file_load_to_memory((void *)LINUX_INITRD_ADDR, argv[0], &initrd_size, RT_NULL))
    {
    case FILE_LOAD_OK:
        if (dtb_size > 0)
        {
            dtb_size = fdt_install_initrd((void *)LINUX_DTB_ADDR, LINUX_INITRD_ADDR, initrd_size);
        }
        rt_kprintf("Initrd size: %d bytes, at address 0x%x ~ 0x%x.\n",
                initrd_size, LINUX_INITRD_ADDR, LINUX_INITRD_ADDR + initrd_size);
    break;
    case FILE_NAME_EMPTY:
        rt_boot_cmd_help("initrd <initrd-path>\n");
    break;
    case FILE_INVALID:
        rt_kprintf("Invalid initrd file.\n");
    break;
    default: break;
    }
}

static void _cpu_prep_linux()
{
    __asm__ volatile (
        "mrs    r0, cpsr\n\r"
        "and    r1, r0, #0x1f\n\r"
        /* set SVC Mode (CPSR_M_SVC = 0x13U) which do not include the ARM virtualization extensions */
        "bicne  r0, r0, #0x1f\n\r"
        "orrne  r0, r0, #0x13\n\r"
        /* disable IRQ, FIQ, cpsr 'I', 'F' bits set 1 [0xc0 = 1100 0000] */
        "orr    r0, r0, #0xc0\n\r"
        "msr    cpsr, r0");

#ifdef RT_USING_SMP
    rt_hw_local_irq_disable();
#else
    rt_hw_interrupt_disable();
#endif

    /* D-cache = off */
    rt_cpu_dcache_clean_flush();
    rt_hw_cpu_dcache_disable();

    /* I-cache = don't care */
    rt_cpu_icache_flush();
    rt_hw_cpu_icache_disable();

    /* MMU = off */
    rt_cpu_mmu_disable();
}

static void __attribute__ ((noreturn)) jump_to_linux()
{
    int i;
    void (*kernel_entry)();

    /* setup atags if dtb not exist */
    if (dtb_size <= 0)
    {
        rt_size_t atag_size;

        /* start of set atags */
        kernel_params = (struct tag *)LINUX_DTB_ADDR;
        _setup_tag_core();
        _setup_tag_initrd(LINUX_INITRD_ADDR, initrd_size);
        _setup_tag_serialnr(BSP_SERIAL_NR >> 32, BSP_SERIAL_NR & 0xffffffff);
        _setup_tag_revision(BSP_MACHINE_REV);
        _setup_tag_mem32(kernel_ram_start, BSP_MACHINE_MEM);
        _setup_tag_cmdline(kernel_cmdline == RT_NULL ? "" : kernel_cmdline);
        _setup_tag_end();
        /* end of set atags */

        atag_size = (rt_ubase_t)kernel_params - LINUX_DTB_ADDR + sizeof(struct tag_header);
        rt_kprintf("ATAGS size: %d bytes, at address 0x%x ~ 0x%x.\n",
                atag_size, LINUX_DTB_ADDR, LINUX_DTB_ADDR + atag_size);
    }

    _cpu_prep_linux();

    /* mask all ppi and spi */
    for (i = 16; i < MAX_HANDLERS; ++i)
    {
        rt_hw_interrupt_mask(i);
    }

    kernel_put_jump_start();

    /*
     *  r0 = 0
     *  r1 = machine type number
     *  r2 = atags or dtb ptr
     */
    kernel_entry = (void(*)(rt_uint32_t r0, rt_uint32_t r1, rt_uint32_t r2))LINUX_KERNEL_ADDR;

    /* call kernel entry, stext */
    kernel_entry(0, BSP_MACHINE_ID, LINUX_DTB_ADDR);

    __builtin_unreachable();
}

#ifdef RT_USING_SMP
static void __attribute__ ((noreturn)) _smp_cpu_init(int irqno, void *param)
{
    /* irqno may not really org irq of interrupt controller */
    rt_hw_interrupt_ack(rt_hw_interrupt_get_irq());

    _cpu_prep_linux();

    __asm__ volatile (
        "   mov     r1, %0\n\r"
        "1: wfe\n\r"
        "   ldr     r0, [r1]\n\r"
        "   cmp     r0, #0\n\r"
        "   blxne   r0\n\r"
        "   b       1b"
        ::"r"(kernel_cpu_release_addr));

    __builtin_unreachable();
}
#endif /* RT_USING_SMP */

void kernel_linux_boot(void)
{
#ifdef RT_USING_SMP
    rt_hw_interrupt_install(RT_BOOT_CPU_SYNC_IPI, _smp_cpu_init, RT_NULL, "cpu_sync");

    rt_hw_ipi_send(RT_BOOT_CPU_SYNC_IPI, RT_CPU_MASK ^ (1 << rt_hw_cpu_id()));
#endif

    jump_to_linux();
}
