/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rtthread.h>

#include <rt-boot/cmd.h>

static char *_quotes_stack_base = RT_NULL;

static void _push_quotes(char **stack, char quotes)
{
    ++(*stack);
    **stack = quotes;
}

static char _pop_quotes(char **stack)
{
    char ret = 0;

    if (*stack > _quotes_stack_base)
    {
        ret = **stack;
        (*stack)--;
    }

    return ret;
}

static rt_bool_t _legal_cmd_char(char c)
{
    if ((c >= '0' && c <= '9') ||
        (c >= 'A' && c <= 'Z') ||
        (c >= 'a' && c <= 'z'))
    {
        return RT_TRUE;
    }

    return RT_FALSE;
}

static rt_bool_t _legal_split_char(char c)
{
    if (c == ';' || c == ' ')
    {
        return RT_TRUE;
    }

    return RT_FALSE;
}

RT_BOOT_CMD(auto, "Run auto commands", char **argv)
{
    int i;
    rt_size_t cmds_len;
    char *cmds_fixed, *cmds_ptr;
    char *cmd, *cmd_argv[RT_BOOT_CMD_ARGS_MAX + 1];
    char *quotes_stack;

    if (rt_boot_auto_cmds == RT_NULL)
    {
        return;
    }

    cmds_len = rt_strlen(rt_boot_auto_cmds);

    if (cmds_len == 0)
    {
        return;
    }

    cmds_len += 1;
    cmds_fixed = cmds_ptr = (char *)rt_malloc(cmds_len);

    /* make sure enough space to save quotes */
    _quotes_stack_base = (char *)rt_malloc(cmds_len);

    if (cmds_fixed == RT_NULL || _quotes_stack_base == RT_NULL)
    {
        rt_kprintf("Memory empty.\n");

        goto _end;
    }

    rt_strcpy(cmds_ptr, rt_boot_auto_cmds);
    cmds_ptr[cmds_len - 1] = '\0';

    while (*cmds_ptr)
    {
        /* skip split */
        for (; _legal_split_char(*cmds_ptr); ++cmds_ptr)
        {
        }

        /* get cmd */
        cmd = cmds_ptr;

        /* check cmd */
        for (;; ++cmds_ptr)
        {
            if (_legal_cmd_char(*cmds_ptr))
            {
                continue;
            }
            else if (_legal_split_char(*cmds_ptr))
            {
                /* end of this cmd */
                *cmds_ptr++ = '\0';
                break;
            }
            else
            {
                if (*cmds_ptr == '\0')
                {
                    break;
                }
                goto _syntax_err;
            }
        }

        /* get argv */
        cmd_argv[0] = cmds_ptr;
        quotes_stack = _quotes_stack_base;

        for (i = 1; *cmds_ptr != '\0'; ++cmds_ptr)
        {
            if (_legal_split_char(*cmds_ptr))
            {
                char last = _pop_quotes(&quotes_stack);

                /* check if in the quotes */
                if (last == 0)
                {
                    char split_char = *cmds_ptr;

                    /* end of this arg, point to next element */
                    *cmds_ptr++ = '\0';

                    if (split_char == ';')
                    {
                        /* end of this cmdline */
                        break;
                    }

                    while (_legal_split_char(*cmds_ptr))
                    {
                        ++cmds_ptr;
                    }
                    /* get next argv */
                    cmd_argv[i++] = cmds_ptr;
                    quotes_stack = _quotes_stack_base;

                    /* Will inc, so dec early */
                    --cmds_ptr;

                    continue;
                }
                _push_quotes(&quotes_stack, last);
            }
            else if (*cmds_ptr == '"' || *cmds_ptr == '\'')
            {
                char last = _pop_quotes(&quotes_stack);

                if (last == 0)
                {
                    /* no quotes before */
                    _push_quotes(&quotes_stack, *cmds_ptr);
                }
                else if (last != *cmds_ptr)
                {
                    /* new quotes */
                    _push_quotes(&quotes_stack, last);
                    _push_quotes(&quotes_stack, *cmds_ptr);

                }
            }
        }

        rt_boot_cmd_exec(cmd, (char **)cmd_argv);
    }

    goto _end;

_syntax_err:
    rt_kprintf("Syntax error:\n\t%s\n", cmds_ptr);

_end:
    if (cmds_fixed != RT_NULL)
    {
        rt_free(cmds_fixed);
    }
    if (_quotes_stack_base != RT_NULL)
    {
        rt_free(_quotes_stack_base);

        _quotes_stack_base = RT_NULL;
    }
}
