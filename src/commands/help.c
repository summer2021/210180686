/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rtthread.h>

#include <rt-boot/cmd.h>

extern char __rt_init_start;
extern char __rt_init_end;

RT_BOOT_CMD_DEFINE(rt_boot_cmd_placeholder, "\n", "rt-boot cmd placeholder", RT_NULL);

static const struct rt_boot_cmd *_cmd_start, *_cmd_end;

static int _rt_boot_cmd_init(void)
{
    _cmd_start = _cmd_end = &rt_boot_cmd_placeholder;

    while ((struct rt_boot_cmd *)&__rt_init_start < _cmd_start)
    {
        --_cmd_start;

        if (_cmd_start->magic != RT_BOOT_CMD_MAGIC)
        {
            ++_cmd_start;
            break;
        }
    }

    while (_cmd_end < (struct rt_boot_cmd *)&__rt_init_end)
    {
        ++_cmd_end;

        if (_cmd_end->magic != RT_BOOT_CMD_MAGIC)
        {
            --_cmd_end;
            break;
        }
    }

    return 0;
}
INIT_APP_EXPORT(_rt_boot_cmd_init);

int rt_boot_cmd_help(const char *info)
{
    return rt_kprintf("Usage: rt_boot %s", info);
}

int rt_boot_cmd_exec(char *name, char **argv)
{
    const struct rt_boot_cmd *rt_boot_cmd_ptr = _cmd_start;

    if (rt_boot_cmd_ptr != RT_NULL)
    {
        while (rt_boot_cmd_ptr <= _cmd_end)
        {
            if (!rt_strcmp(rt_boot_cmd_ptr->name, name))
            {
                rt_boot_cmd_ptr->handler(argv);

                return 0;
            }
            ++rt_boot_cmd_ptr;
        }
    }

    return -1;
}

RT_BOOT_CMD(help, "Display this information", char **argv)
{
    const struct rt_boot_cmd *rt_boot_cmd_ptr = _cmd_start;

    if (rt_boot_cmd_ptr != RT_NULL)
    {
        while (rt_boot_cmd_ptr <= _cmd_end)
        {
            if (rt_boot_cmd_ptr != &rt_boot_cmd_placeholder)
            {
                rt_kprintf("%-8s - %s.\n", rt_boot_cmd_ptr->name, rt_boot_cmd_ptr->info);
            }
            ++rt_boot_cmd_ptr;
        }
    }
}
