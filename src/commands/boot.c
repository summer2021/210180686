/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rtthread.h>

#include <rt-boot/cmd.h>
#include <rt-boot/kernel.h>

static rt_uint32_t _kernel_type_current = KERNEL_TYPE_EMPTY;

void kernel_put_no_load(void)
{
    rt_kprintf("You need to load the kernel first.\n");
}

void kernel_put_jump_start(void)
{
    rt_kprintf("\rJump to kernel...\n");
}

void kernel_set_type(int type)
{
    _kernel_type_current = type;
}

extern void kernel_linux_boot(void);

RT_BOOT_CMD(boot, "Boot kernel", char **argv)
{
    switch (_kernel_type_current)
    {
    case KERNEL_TYPE_LINUX:
        kernel_linux_boot();
    break;
    case KERNEL_TYPE_EMPTY:
        kernel_put_no_load();
    break;
    default:
        rt_kprintf("Unsupported kernel.\n");
    break;
    }
}
