#include <rtconfig.h>
#include <rtthread.h>

#include <rt-boot/cmd.h>

#ifdef RT_USING_LWIP
#include <lwip/sockets.h>
#include <lwip/netdb.h>
#include <lwip/inet.h>

#include <unistd.h>
#include <fcntl.h>

#define TFTP_RRQ    1
#define TFTP_WRQ    2
#define TFTP_DATA   3
#define TFTP_ACK    4
#define TFTP_ERR    5

#define DATA_BUF_SZ  512
#define TFTP_HEAD_SZ 4
#define DEFAULT_PORT 69
#endif /* RT_USING_LWIP */

RT_BOOT_CMD(tftp, "Get file from tftp server", char **argv)
{
#ifdef RT_USING_LWIP
    int sock = -1;
    char host[sizeof("255.255.255.255") / sizeof(char) - 1];
    int port = DEFAULT_PORT;
    struct sockaddr_in server_addr, reply_addr;
    struct hostent *get_hostent;
    socklen_t reply_addr_sz = sizeof(struct sockaddr_in);

    char *send_buf = RT_NULL, *recv_buf = RT_NULL;
    int data_sz, data_total_sz = 0;
    int mb_sz = -1, ack_sz = 1;
    int file_fd = -1;

    if (argv[0] == RT_NULL || argv[1] == RT_NULL || argv[2] == RT_NULL)
    {
        rt_boot_cmd_help("tftp <host[:port]> <tftp-filename> <save-filename>\n");
        goto _end;
    }
    else
    {
        int i = 0;
        char *addr = argv[0];
        while (*addr)
        {
            if (*addr != ':')
            {
                host[i++] = *addr;
            }
            else
            {
                host[i] = '\0';
                if (*++addr != '\0')
                {
                    port = atoi(addr);
                }
                break;
            }
            ++addr;
        }
    }

    file_fd = open(argv[2], O_CREAT | O_RDWR | O_TRUNC, 0);

    if (file_fd == -1)
    {
        rt_kprintf("File name `%s' is invalid.\n", argv[2]);
        return;
    }

    if ((get_hostent = lwip_gethostbyname(host)) == RT_NULL)
    {
        rt_kprintf("Host name `%s' error.\n", host);
        /* get_hostent no need to free */
        goto _end;
    }

    if ((sock = lwip_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        rt_kprintf("Init socket fail.\n");
        goto _end;
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = lwip_htons(port);
    server_addr.sin_addr = *((struct in_addr *)get_hostent->h_addr);

    send_buf = (char *)rt_malloc(sizeof(char) * (DATA_BUF_SZ + TFTP_HEAD_SZ));
    recv_buf = (char *)rt_malloc(sizeof(char) * (DATA_BUF_SZ + TFTP_HEAD_SZ));
    if (send_buf == RT_NULL || recv_buf == RT_NULL)
    {
        rt_kprintf("\nMemory(%d bytes) is not enough.\n",  (DATA_BUF_SZ + TFTP_HEAD_SZ));
        goto _end;
    }

    rt_memset(send_buf, 0, (DATA_BUF_SZ + TFTP_HEAD_SZ));
    send_buf[0] = 0;
    send_buf[1] = TFTP_RRQ;
    rt_strcpy(send_buf + 2, argv[1]);
    rt_strcpy(send_buf + 2 + rt_strlen(argv[1]) + 1, "octet");
    rt_kprintf("Sending read request...\n");
    if (lwip_sendto(sock, send_buf, DATA_BUF_SZ, 0, (struct sockaddr *)&server_addr, sizeof(struct sockaddr_in)) == -1)
    {
        rt_kprintf("Connection refused.\n");
        goto _end;
    }

    rt_kprintf("Receiving...\n");
    for (;;)
    {
        rt_memset(recv_buf, 0, (DATA_BUF_SZ + TFTP_HEAD_SZ));
        data_sz = lwip_recvfrom(sock, recv_buf, (DATA_BUF_SZ + TFTP_HEAD_SZ), 0, (struct sockaddr *)&reply_addr, &reply_addr_sz);

        if (data_sz == -1 || recv_buf[1] == TFTP_ERR)
        {
            rt_kprintf("Received error.\n");
            goto _end;
        }
        data_sz -= TFTP_HEAD_SZ;

        data_total_sz += data_sz;
        if (data_total_sz >> 20 > mb_sz)
        {
            ++mb_sz;
            rt_kprintf("\r%dMB", mb_sz);
        }
/* disable lwip_write macros */
#undef write
        write(file_fd, &recv_buf[4], data_sz);

        rt_memset(send_buf, 0, (DATA_BUF_SZ + TFTP_HEAD_SZ));
        recv_buf[0] = 0x0;
        recv_buf[1] = TFTP_ACK;
        recv_buf[2] = ack_sz >> 8;
        recv_buf[3] = ack_sz % (0xff + 1);

        if (lwip_sendto(sock, recv_buf, TFTP_HEAD_SZ, 0 , (struct sockaddr *)&reply_addr, reply_addr_sz) == -1)
        {
            rt_kprintf("Send ACK fail.\n");
            goto _end;
        }

        ++ack_sz;
        if (data_sz < DATA_BUF_SZ)
        {
            break;
        }
    }

    rt_kprintf("\nGet file end, size: %d bytes.\n", data_total_sz);

_end:
    if (sock != -1)
    {
        lwip_close(sock);
    }
/* disable lwip_close macros */
#undef close
    if (file_fd != -1)
    {
        close(file_fd);
    }
    if (send_buf != RT_NULL)
    {
        rt_free(send_buf);
    }
    if (recv_buf != RT_NULL)
    {
        rt_free(recv_buf);
    }
#else
    rt_kprintf("No configured `RT_USING_LWIP'.\n");
#endif /* RT_USING_LWIP */
}