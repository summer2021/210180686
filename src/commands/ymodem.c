#include <rtconfig.h>
#include <rtthread.h>

#include <rt-boot/cmd.h>

#ifdef YMODEM_USING_FILE_TRANSFER
#include <ymodem.h>

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

static rt_size_t file_sz;
static rt_size_t file_cur;
static int file_fd;
static rt_uint8_t* file_buf;

static enum rym_code ymodem_on_begin(struct rym_ctx *ctx, rt_uint8_t *buf, rt_size_t len)
{
    /* data: [file-name][file-size] */
    file_sz = atol((char *)&buf[rt_strlen((char *)&buf[0]) + 1]);
    file_buf = rt_malloc(sizeof(char) * file_sz);
    file_cur = 0;

    if (file_buf == RT_NULL)
    {
        rt_kprintf("\nMemory(%d bytes) is not enough.", file_sz);
        return RYM_CODE_CAN;
    }

    return RYM_CODE_ACK;
}

static enum rym_code ymodem_on_data(struct rym_ctx *ctx, rt_uint8_t *buf, rt_size_t len)
{
    int i = 0;

    while (i < len)
    {
        if (file_cur > file_sz)
        {
            return RYM_CODE_CAN;
        }
        file_buf[file_cur++] = buf[i++];
    }

    return RYM_CODE_ACK;
}

#endif /* YMODEM_USING_FILE_TRANSFER */

RT_BOOT_CMD(ymodem, "Get file from serial port with ymodem", char **argv)
{
#ifdef YMODEM_USING_FILE_TRANSFER
    struct rym_ctx rctx;

    if (argv[0] == RT_NULL)
    {
        rt_boot_cmd_help("ymodem <filename>\n");
        return;
    }

    file_fd = open(argv[0], O_CREAT | O_RDWR | O_TRUNC, 0);

    if (file_fd == -1)
    {
        rt_kprintf("File name `%s' is invalid.\n", argv[0]);
        return;
    }

    rt_kprintf("Connecting...\n");
    rym_recv_on_device(
            &rctx,
            rt_console_get_device(),
            RT_DEVICE_OFLAG_RDWR | RT_DEVICE_FLAG_INT_RX,
            ymodem_on_begin,
            ymodem_on_data,
            RT_NULL,
            RT_TICK_PER_SECOND);

    if (file_cur > file_sz)
    {
        write(file_fd, file_buf, file_sz);
        rt_kprintf("\n\rGet file success, size: %d bytes.\n\r", file_sz);
    }
    else
    {
        rt_kprintf("\n\rGet file fail.\n\r");
    }

    close(file_fd);
    if (file_buf != RT_NULL)
    {
        rt_free(file_buf);
    }

    /* wait some time for terminal response finish */
    rt_thread_delay(RT_TICK_PER_SECOND);

    return;
#else
    rt_kprintf("No configured `YMODEM_USING_FILE_TRANSFER'.\n");
#endif /* YMODEM_USING_FILE_TRANSFER */
}
