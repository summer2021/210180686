/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#ifndef __RT_BOOT_CONFIG_H__
#define __RT_BOOT_CONFIG_H__

#include <rtconfig.h>

const char *rt_boot_auto_cmds =
#ifdef SOC_VEXPRESS_A9
    "linux zImage 'root=/dev/mmcblk0 console=ttyAMA0 earlycon rw';"
    "dtb vexpress-v2p-ca9.dtb;"
    "initrd initrd.img;"
    "boot"
#endif
#ifdef SOC_VIRT64_AARCH64
    "linux Image.gz 'root=/dev/mmcblk0 console=ttyAMA0 earlycon rw';"
    "dtb virt-aarch64.dtb;"
    "initrd initrd.img;"
    "boot"
#endif
#ifdef BCM2836_SOC
    "linux kernel8.img 'root=/dev/mmcblk0 console=ttyAMA0 earlycon=pl011,0x3f201000 rw';"
    "dtb raspi3b.dtb;"
    "initrd initrd.img;"
    "boot"
#endif
#ifdef BCM2711_SOC
    "linux kernel8.img 'coherent_pool=1M 8250.nr_uarts=1 snd_bcm2835.enable_compat_alsa=0 snd_bcm2835.enable_hdmi=1"
        " bcm2708_fb.fbwidth=0 bcm2708_fb.fbheight=0 bcm2708_fb.fbswap=1 smsc95xx.macaddr=E4:5F:01:43:5F:18"
        " vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000 console=ttyAMA0,115200"
        " console=tty1 root=/dev/mmcblk0p2 rootfstype=squashfs,ext4 rootwait earlycon=pl011,mmio32,0xfe201000';"
    "dtb raspi4b.dtb;"
    "boot"
#endif
"";

#endif /* __RT_BOOT_CONFIG_H__ */
