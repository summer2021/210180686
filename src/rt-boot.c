/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rtthread.h>

#include <rt-boot/cmd.h>

#include "config.h"

#ifdef PV_OFFSET
#if    PV_OFFSET != 0
#error PV_OFFSET != 0
#endif
#endif

static void rt_boot(rt_uint8_t argc, char **argv)
{
    if (argc > 1)
    {
        if (rt_boot_cmd_exec(argv[1], &argv[2]))
        {
            rt_kprintf("Options '%s' is not supported.\n", argv[1]);
        }
        return;
    }

    rt_kprintf("Usage: rt_boot [options] arguments...\n");
}
MSH_CMD_EXPORT(rt_boot, kernel bootloader.);
