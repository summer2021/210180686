/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rtthread.h>

/* https://tools.ietf.org/html/rfc1951 */

#include <rt-boot/utils/gzip.h>

#define BUFFER_SIZE                 1024
#define NO_VALUE                    ((rt_uint16_t)(~0U))

#define GZIP_HEADER_SIZE            10
#define GZIP_MAGIC                  0x8b1f
#define GZIP_DEFLATE_CM             0x08

#define DEFLATE_LITERAL_BLOCK_TYPE  0
#define DEFLATE_FIX_HUF_BLOCK_TYPE  1
#define DEFLATE_DYN_HUF_BLOCK_TYPE  2
#define DEFLATE_CODE_MAX_BIT_LENGTH 32
#define DEFLATE_ALPHABET_SIZE       288
#define DEFLATE_END_BLOCK_VALUE     256

#define DYNAMIC_DICT_SIZE           65535

typedef struct
{
    rt_uint8_t bfinal:1;
    rt_uint8_t btype:2;
    rt_uint8_t *data;
} block_t;

#define FTEXT       1
#define FHCRC       (1 << 1)
#define FEXTRA      (1 << 2)
#define FNAME       (1 << 3)
#define FCOMMENT    (1 << 4)

typedef struct
{
    rt_uint8_t code_lengths[DEFLATE_ALPHABET_SIZE];
    rt_uint32_t next_codes[32];
} static_huffman_params_t;

/* [8] * (144 - 0) + [9] * (256-144) + [7] * (280 - 256) + [8] * (288 - 280) */
static static_huffman_params_t static_huffman_params =
{
    {
        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        9, 9, 9, 9, 9, 9, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
        7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8
    },
    {
        0, 0, 0, 0, 0, 0, 0, 0b0000000, 0b00110000, 0b110010000
    },
};


#define DEFLATE_STATIC_DISTANCE_CODE_LENGTHS_SIZE 32
#define DEFLATE_SDCLS DEFLATE_STATIC_DISTANCE_CODE_LENGTHS_SIZE
static rt_uint8_t static_huffman_params_distance_code_lengths[DEFLATE_SDCLS] =
{
    5, 5, 5, 5, 5, 5, 5, 5,
    5, 5, 5, 5, 5, 5, 5, 5,
    5, 5, 5, 5, 5, 5, 5, 5,
    5, 5, 5, 5, 5, 5, 5, 5
};

#define DEFLATE_LENGTH_EXTRA_BITS_ARRAY_SIZE    29
#define DEFLATE_LENGTH_EXTRA_BITS_ARRAY_OFFSET  257

static rt_uint16_t length_lookup[DEFLATE_LENGTH_EXTRA_BITS_ARRAY_SIZE] =
{
    3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31,
    35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258
};

static rt_uint8_t length_extra_bits[DEFLATE_LENGTH_EXTRA_BITS_ARRAY_SIZE] =
{
    0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2,
    2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0
};

#define DEFLATE_DISTANCE_EXTRA_BITS_ARRAY_SIZE 30

static rt_uint16_t distance_lookup[DEFLATE_DISTANCE_EXTRA_BITS_ARRAY_SIZE] =
{
    1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385,
    513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577
};

static rt_uint8_t distance_extra_bits[DEFLATE_DISTANCE_EXTRA_BITS_ARRAY_SIZE] =
{
    0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6,
    7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13
};


#define CODE_LENGTHS_CODE_LENGTH 19
static rt_uint8_t code_length_code_alphabet[CODE_LENGTHS_CODE_LENGTH] =
{
    16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15
};

static rt_uint8_t code_length_lengths_extra_size[CODE_LENGTHS_CODE_LENGTH] =
{
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 7
};

static rt_uint8_t code_length_lengths_extra_size_offset[CODE_LENGTHS_CODE_LENGTH] =
{
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 11
};

static rt_uint8_t *g_buf = RT_NULL;
static rt_uint8_t *g_output = RT_NULL;

#define INCREMENT_MASK(mask, ptr) \
    mask = mask << 1 | mask >> 7; \
    ptr += mask & 1;

#define READ(dest, mask, ptr, size) \
{                                           \
    dest = 0;                               \
    rt_uint32_t _pos = 1;                   \
    rt_uint8_t _size = size;                \
    while (_size--)                         \
    {                                       \
        dest |= (*ptr & mask ? _pos : 0);   \
        _pos <<= 1;                         \
        INCREMENT_MASK(mask, ptr)           \
    }                                       \
}

typedef rt_uint16_t *dict_t;

static char *_strndup(const char *s, rt_size_t size)
{
    rt_size_t nsize = rt_strnlen(s, size);
    char *news = (char *)rt_malloc(nsize + 1);

    if (news)
    {
        rt_memcpy(news, s, nsize);
        news[nsize] = '\0';
    }

    return news;
}

static rt_uint8_t *get_extra_header(rt_uint8_t *buf, gzip_header_t header, gzip_extra_header_t *extra)
{
    rt_uint8_t *current = buf + GZIP_HEADER_SIZE;
    if (header.flg & FEXTRA)
    {
        rt_uint16_t xlen = *current;
        extra->xlen = xlen;
        current += 2 + xlen;
    }
    if (header.flg & FNAME)
    {
        rt_uint8_t *beg = current;
        while (*current++ != 0) ;
        extra->fname = _strndup((const char *) beg, current - beg);
    }
    if (header.flg & FCOMMENT)
    {
        rt_uint8_t *beg = current;
        while (*current++ != 0) ;
        extra->fcomment = _strndup((const char *) beg, current - beg);
    }
    if (header.flg & FHCRC)
    {
        rt_uint16_t crc16 = *current;
        extra->crc16 = crc16;
        current += 2;
    }
    return current;
}

int gzip_get_metadata(rt_uint8_t *buf, ssize_t size, gzip_metadata_t *metadata)
{
    int status;

    rt_memcpy(&metadata->header, buf, GZIP_HEADER_SIZE);
    rt_memset(&metadata->extra_header, 0, sizeof (gzip_extra_header_t));

    rt_uint8_t *pos = get_extra_header(buf, metadata->header, &metadata->extra_header);

    metadata->footer.crc32 = buf[size - 8] | buf[size - 7] << 8 | buf[size - 6] << 16 | buf[size - 5] << 24;
    metadata->footer.isize = buf[size - 4] | buf[size - 3] << 8 | buf[size - 2] << 16 | buf[size - 1] << 24;

    if (metadata->header.magic != GZIP_MAGIC)
    {
        status = 1;
        goto _err;
    }
    if (metadata->header.cm != GZIP_DEFLATE_CM)
    {
        status = -1;
        goto _err;
    }

    metadata->block_offset = pos - buf;

    return 0;
_err:
    gzip_abandon_metadata(metadata);
    return status;
}

void gzip_abandon_metadata(gzip_metadata_t *metadata)
{
    if (metadata->extra_header.fname != RT_NULL)
    {
        rt_free(metadata->extra_header.fname);
    }
    if (metadata->extra_header.fcomment != RT_NULL)
    {
        rt_free(metadata->extra_header.fcomment);
    }
}

/**
 * Counts the number of code by length.
 * If { 2, 1, 3, 3 } represents the code lengths then there is one code of
 * length 2, 1 of length 1 and 2 of length 3. The function will fillup
 * bit_counts with the result this way: { 0, 1, 1, 2, 0 ...., 0 }.
 *
 * @params code_lengths is the array of code length
 * @params size is the size of code_lengths
 * @params length_counts is the resulting array. The array must be allocated
 * with a minimum size of DEFLATE_CODE_MAX_BIT_LENGTH.
 */
static void count_by_code_length(const rt_uint8_t *code_lengths, ssize_t size, rt_uint8_t *length_counts)
{
    rt_memset(length_counts, 0, DEFLATE_CODE_MAX_BIT_LENGTH * sizeof (rt_uint8_t));

    for (; size > 0; --size)
    {
        length_counts[code_lengths[size - 1]]++;
    }
    length_counts[0] = 0;
}

/**
 * Generates the starting code for a specific code lengths.
 * if length_counts = { 0, 1, 1, 2 } the next_codes is { 0, 2, 6 }, representing
 * the following dictionary:
 * value code
 * ----- ----
 * A     10  -> A is encoded on two bits, first code is 2 (10)
 * B     0   -> B is encoded on one bit, first code is 0 (0)
 * C     110 -> C is encoded on three bits, first code is 110 (6)
 * D     111 -> D is encoded on three bits, code is 111 (6 + 1). This code is
 *              not present in next_codes as it is not the first code of its
 *              length
 * https://tools.ietf.org/html/rfc1951#page-8
 *
 * @param length_counts length_counts[N] is the number of code of length N.
 * Its size is DEFLATE_CODE_MAX_BIT_LENGTH.
 * @param next_codes  next_codes[N] is the first code of length N. It must be
 * allocated with a minimum size of DEFLATE_CODE_MAX_BIT_LENGTH.
 */
static void generate_next_codes(rt_uint8_t *length_counts, rt_uint32_t *next_codes)
{
    rt_uint32_t code = 0;
    rt_uint8_t nbits;

    rt_memset(next_codes, 0, DEFLATE_CODE_MAX_BIT_LENGTH * sizeof (rt_uint32_t));
    for (nbits = 1; nbits < DEFLATE_CODE_MAX_BIT_LENGTH; ++nbits)
    {
        next_codes[nbits] = code = (code + length_counts[nbits - 1]) << 1;
    }
}

/**
 * Generates an containg the mapped value.
 * @params code_lengths is the array of code length
 * @params size is the size of code_lengths
 * @param next_codes  next_codes[N] is the first code of length N. It must be
 * allocated with a minimum size of DEFLATE_CODE_MAX_BIT_LENGTH.
 * @params dict A array filled up with values depending on their huffman
 * code. For example:
 * { A: 010, B: 00, C: 10 }
 * is represented with this tree:
 *      x
 *    /   \
 *   x     x
 *  / \   /
 * B   x C
 *    /
 *   A
 * which in turn is stored in this array:
 * { -1, -1, -1, B, -1, C, -1, -1, -1, A, -1, -1, -1, -1, -1 }
 */
static void generate_dict(const rt_uint8_t *code_lengths, ssize_t size, rt_uint32_t *next_codes, dict_t dict, rt_uint16_t dict_size)
{
    rt_uint16_t i;

    rt_memset(dict, NO_VALUE, dict_size * sizeof (rt_uint16_t));

    for (i = 0; i < size; ++i)
    {
        rt_uint8_t length = code_lengths[i];
        if (length == 0)
        {
            continue;
        }
        rt_uint32_t code = next_codes[length];
        rt_uint32_t m = 1 << (length - 1);
        rt_uint16_t index = 0;
        while (m)
        {
            index <<= 1;
            index += code & m ? 2 : 1;
            m >>= 1;
        }
        dict[index] = i;
        next_codes[code_lengths[i]]++;
    }
}

static void generate_dict_from_code_length(const rt_uint8_t *code_lengths, rt_size_t length_counts_size, dict_t dict, rt_uint32_t dict_size)
{
    rt_uint8_t length_counts[DEFLATE_CODE_MAX_BIT_LENGTH];
    count_by_code_length(code_lengths, length_counts_size, length_counts);

    rt_uint32_t next_codes[DEFLATE_CODE_MAX_BIT_LENGTH];
    generate_next_codes(length_counts, next_codes);

    generate_dict(code_lengths, length_counts_size, next_codes, dict, dict_size);
}

static void decode_dynamic_dict_lengths(rt_uint8_t **buf, rt_uint8_t *mask, rt_size_t output_size, dict_t dict, rt_uint8_t *output)
{
    rt_uint16_t index = 0;
    rt_uint16_t value = 0;

    while (output_size)
    {
        do
        {
            index <<= 1;
            index += **buf & *mask ? 2 : 1;
            INCREMENT_MASK(*mask, *buf);
        } while ((value = dict[index]) == NO_VALUE);

        index = 0;

        if (value < 16)
        {
            *output++ = value;
            --output_size;
        }
        else
        {
            if (value == 16)
            {
                rt_uint8_t extra = 0;
                READ(extra, *mask, *buf, 2);
                rt_uint8_t last_value = *(output - 1);
                for (int i = 0; i < extra + 3; ++i)
                {
                    *output++ = last_value;
                    --output_size;
                }
            }
            else
            {
                rt_uint8_t extra = 0;
                rt_uint8_t extra_size = code_length_lengths_extra_size[value];
                int i;

                READ(extra, *mask, *buf, extra_size);

                for (i = 0; i < extra + code_length_lengths_extra_size_offset[value]; ++i)
                {
                    *output++ = 0;
                    --output_size;
                }
            }
        }
    }
}

static void parse_dynamic_tree(rt_uint8_t **buf, rt_uint8_t *mask,
        dict_t litdict, dict_t distdict)
{
    rt_uint8_t hlen;
    rt_uint8_t hdist;
    rt_uint8_t hlit;
    rt_uint8_t i;
    rt_uint8_t code_length_lengths[CODE_LENGTHS_CODE_LENGTH];
    static rt_uint16_t code_length_dict[256];
    static rt_uint8_t literal_lengths[287];
    static rt_uint8_t distance_lengths[32];

    READ(hlit, *mask, *buf, 5);
    READ(hdist, *mask, *buf, 5);
    READ(hlen, *mask, *buf, 4);

    rt_memset(code_length_lengths, 0, CODE_LENGTHS_CODE_LENGTH * sizeof (rt_uint8_t));
    for (i = 0; i < hlen + 4; ++i)
    {
        READ(code_length_lengths[code_length_code_alphabet[i]], *mask, *buf, 3);
    }

    generate_dict_from_code_length(code_length_lengths, CODE_LENGTHS_CODE_LENGTH, code_length_dict, 256);

    rt_memset(literal_lengths, 0, 287 * sizeof (rt_uint8_t));
    decode_dynamic_dict_lengths(buf, mask, hlit + 257, code_length_dict, literal_lengths);

    rt_memset(distance_lengths, 0, 32 * sizeof (rt_uint8_t));
    decode_dynamic_dict_lengths(buf, mask, hdist + 1, code_length_dict, distance_lengths);

    generate_dict_from_code_length(literal_lengths, hlit + 257, litdict, DYNAMIC_DICT_SIZE);
    generate_dict_from_code_length(distance_lengths, hdist + 1, distdict, DYNAMIC_DICT_SIZE);
}

static rt_uint8_t * inflate_block(rt_uint8_t **buf, rt_uint8_t *mask, dict_t litdict, dict_t distdict, rt_uint8_t *output)
{
    rt_uint16_t index = 0;
    rt_uint16_t value = 0;

    while (value != DEFLATE_END_BLOCK_VALUE)
    {
        do
        {
            index = (index << 1) + 1;
            if ((**buf & *mask) != 0)
            {
                ++index;
            }
            INCREMENT_MASK(*mask, *buf);
        } while ((value = litdict[index]) == NO_VALUE);

        if (value < DEFLATE_END_BLOCK_VALUE)
        {
            *output++ = value;
        }

        if (value > DEFLATE_END_BLOCK_VALUE)
        {
            rt_uint16_t length = length_lookup[value - DEFLATE_END_BLOCK_VALUE - 1];
            rt_uint8_t nb_extra_bits = length_extra_bits[value - DEFLATE_END_BLOCK_VALUE - 1];
            rt_uint16_t extra_bits = 0;
            rt_uint16_t distance;

            READ(extra_bits, *mask, *buf, nb_extra_bits);
            length += extra_bits;
            index = 0;

            do
            {
                index <<= 1;
                index += **buf & *mask ? 2 : 1;
                INCREMENT_MASK(*mask, *buf);
            } while ((value = distdict[index]) == NO_VALUE);

            distance = distance_lookup[value];
            nb_extra_bits = distance_extra_bits[value];
            extra_bits = 0;
            READ(extra_bits, *mask, *buf, nb_extra_bits);
            distance += extra_bits;

            if (length > distance)
            {
                while (length--)
                {
                    *output = *(output - distance);
                    ++output;
                }
            }
            else
            {
                rt_memcpy(output, output - distance, length);
                output += length;
            }
        }
        index = 0;
    }
    return output;
}

void gzip_inflate(rt_uint8_t *buf, rt_uint8_t *output)
{
    g_buf = buf;
    g_output = output;

    static rt_uint16_t static_dict[1024];
    static rt_uint16_t distance_static_dict[64];
    rt_uint8_t bfinal = 0;
    rt_uint8_t *current_buf = buf;
    rt_uint8_t mask = 1;
    rt_uint8_t *current_output = output;

    generate_dict(static_huffman_params.code_lengths, DEFLATE_ALPHABET_SIZE, static_huffman_params.next_codes, static_dict, 1024);

    rt_memset(distance_static_dict, -1, 64 * sizeof (rt_uint16_t));
    generate_dict_from_code_length(static_huffman_params_distance_code_lengths, 32, distance_static_dict, 32);

    do
    {
        rt_uint8_t btype;

        READ(bfinal, mask, current_buf, 1);
        READ(btype, mask, current_buf, 2);

        switch (btype)
        {
        case DEFLATE_LITERAL_BLOCK_TYPE:
        {
            rt_uint16_t len;
            if (mask != 1)
            {
                current_buf++;
            }

            len = *current_buf | *(current_buf + 1) << 8;
            current_buf += 4;
            rt_memcpy(current_output, current_buf, len * sizeof (rt_uint8_t));
            current_buf += len;
            current_output += len;
            mask = 1;
        }
        break;
        case DEFLATE_FIX_HUF_BLOCK_TYPE:
        {
            current_output = inflate_block(&current_buf, &mask, static_dict, distance_static_dict, current_output);
        }
        break;
        case DEFLATE_DYN_HUF_BLOCK_TYPE:
        {
            static rt_uint16_t dict[DYNAMIC_DICT_SIZE];
            static rt_uint16_t dist_dict[DYNAMIC_DICT_SIZE];
            parse_dynamic_tree(&current_buf, &mask, dict, dist_dict);
            current_output = inflate_block(&current_buf, &mask, dict, dist_dict, current_output);
        }
        break;
        }
    } while (bfinal != 1);
}

unsigned long gzip_crc32(unsigned char *buf, int len)
{
    static unsigned long table[256], crc;
    int i, j;

    for (i = 0; i < 256; ++i)
    {
        crc = i;
        for (j = 0; j < 8; ++j)
        {
            if (crc & 1)
            {
                crc = 0xedb88320L ^ (crc >> 1);
            }
            else
            {
                crc >>= 1;
            }
        }
        table[i] = crc;
    }

    crc = 0xffffffffL;

    for (i = 0; i < len; i++)
    {
        crc = table[(crc ^ buf[i]) & 0xff] ^ (crc >> 8);
    }

    return crc ^ 0xffffffffL;
}
