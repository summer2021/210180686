/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rthw.h>

#include <rt-boot/utils/file.h>

enum file_status file_load_to_memory(void *load_addr, char *filename, rt_size_t *size, int (*file_check_cb)(int fd))
{
    int fd = -1;
    enum file_status status;

    if (filename == RT_NULL)
    {
        status = FILE_NAME_EMPTY;
        goto _end;
    }

    fd = open(filename, O_RDONLY, 0);

    if (fd == -1)
    {
        rt_kprintf("File `%s' not found.\n", filename);
        status = FILE_NOT_FOUND;
        goto _end;
    }

    *size = lseek(fd, 0, SEEK_END);

    if (*size > 0 && (file_check_cb == RT_NULL || !file_check_cb(fd)))
    {
        lseek(fd, 0, SEEK_SET);
        read(fd, load_addr, sizeof(char) * *size);
        status = FILE_LOAD_OK;
    }
    else
    {
        status = FILE_INVALID;
    }

    close(fd);

_end:
    return status;
}
