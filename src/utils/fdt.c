/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#include <rtthread.h>

#include <rt-boot/utils/fdt.h>

#ifndef RT_CPUS_NR
#define RT_CPUS_NR 1
#endif

int _find_and_add_subnode(void *fdt, char *node_name)
{
    int chosen_offset = 0;

    chosen_offset = fdt_subnode_offset(fdt, 0, node_name);

    if (chosen_offset == -FDT_ERR_NOTFOUND)
    {
        chosen_offset = fdt_add_subnode(fdt, 0, node_name);
    }

    return chosen_offset;
}

int fdt_install_cmdline(void *fdt, char *cmdline, int cmdline_sz)
{
    int chosen_offset = _find_and_add_subnode(fdt, "chosen");

    if (chosen_offset >= 0 || chosen_offset == -FDT_ERR_EXISTS)
    {
        if (fdt_setprop(fdt, chosen_offset, "bootargs", cmdline, cmdline_sz) < 0)
        {
            fdt_open_into(fdt, fdt, fdt_totalsize(fdt) + FDT_PADDING_SIZE);
            fdt_setprop(fdt, chosen_offset, "bootargs", cmdline, cmdline_sz);
        }
    }

    return fdt_totalsize(fdt);
}

int fdt_install_initrd(void *fdt, rt_ubase_t initrd_addr, rt_size_t initrd_sz)
{
    rt_uint64_t addr, sz_ptr;
    int chosen_offset = _find_and_add_subnode(fdt, "chosen");
    int i;

    /* update the entry */
    for (i = fdt_num_mem_rsv(fdt) - 1; i >= 0; --i)
    {
        fdt_get_mem_rsv(fdt, i, &addr, &sz_ptr);
        if (addr == initrd_addr)
        {
            fdt_del_mem_rsv(fdt, i);
            break;
        }
    }

    /* add the memory */
    if (fdt_add_mem_rsv(fdt, initrd_addr, initrd_sz) < 0)
    {
        /* move the memory */
        fdt_open_into(fdt, fdt, fdt_totalsize(fdt) + FDT_PADDING_SIZE);
        if (fdt_add_mem_rsv(fdt, initrd_addr, initrd_sz) < 0)
        {
            goto _end;
        }
    }

    /* install initrd */
    if (chosen_offset >= 0 || chosen_offset == -FDT_ERR_EXISTS)
    {
        chosen_offset = fdt_path_offset(fdt, "/chosen");

        fdt_setprop_arch(fdt, chosen_offset, "linux,initrd-start", initrd_addr);
        fdt_setprop_arch(fdt, chosen_offset, "linux,initrd-end", initrd_addr + initrd_sz);
    }

_end:
    return fdt_totalsize(fdt);
}

int fdt_install_spin_table(void *fdt, rt_ubase_t cpu_release_addr)
{
    int i;
    int cpus_offset = fdt_path_offset(fdt, "/cpus"), cpu_offset;
    const char *prop;

    if (cpus_offset < 0)
    {
        goto _end;
    }

    for (cpu_offset = fdt_first_subnode(fdt, cpus_offset), i = 0;
         cpu_offset >= 0 && i < RT_CPUS_NR;
         cpu_offset = fdt_next_subnode(fdt, cpu_offset))
    {
        prop = fdt_getprop(fdt, cpu_offset, "device_type", RT_NULL);

        if (prop == RT_NULL || rt_strcmp(prop, "cpu"))
        {
            continue;
        }

        /* increase cpu nr */
        ++i;

        /*
         * we just continue when cpu using other enable-methods, such as psci, acpi.
         * but we do that the kernel set second cpus on may also fail.
         */
        prop = fdt_getprop(fdt, cpu_offset, "enable-method", NULL);

        if (prop == RT_NULL || strcmp(prop, "spin-table"))
        {
            continue;
        }

        if (fdt_setprop_arch(fdt, cpu_offset, "cpu-release-addr", cpu_release_addr))
        {
            goto _end;
        }
    }

_end:
    return fdt_totalsize(fdt);
}
