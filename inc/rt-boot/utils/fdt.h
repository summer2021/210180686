/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#ifndef __RT_BOOT_UTILS_FDT_H__
#define __RT_BOOT_UTILS_FDT_H__

#include <rtdef.h>

#include <rt-boot/utils/libfdt/libfdt_env.h>

#include <rt-boot/utils/libfdt/fdt.h>
#include <rt-boot/utils/libfdt/libfdt.h>
#include <rt-boot/macros.h>

#define FDT_PADDING_SIZE (1 * KB)

#ifdef ARCH_CPU_64BIT
#define fdt_setprop_arch fdt_setprop_u64
#else
#define fdt_setprop_arch fdt_setprop_u32
#endif

int fdt_install_cmdline(void *fdt, char *cmdline, int cmdline_sz);
int fdt_install_initrd(void *fdt, rt_ubase_t initrd_addr, rt_size_t initrd_sz);
int fdt_install_spin_table(void *fdt, rt_ubase_t cpu_release_addr);

#endif /* __RT_BOOT_UTILS_FDT_H__ */
