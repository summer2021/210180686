/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#ifndef __RT_BOOT_ATAG_H__
#define __RT_BOOT_ATAG_H__

#include <rtdef.h>

#define COMMAND_LINE_SIZE 1024

#define ATAG_NONE       0x00000000  /* the list ends with an ATAG_NONE node */
#define ATAG_CORE       0x54410001  /* the list must start with an ATAG_CORE node */
#define ATAG_MEM        0x54410002  /* it is allowed to have multiple ATAG_MEM nodes */
#define ATAG_VIDEOTEXT  0x54410003  /* vga text type displays */
#define ATAG_RAMDISK    0x54410004  /* describes how the ramdisk will be used in kernel */
#define ATAG_INITRD     0x54410005  /* describes where the compressed ramdisk image lives (virtual address) */
#define ATAG_INITRD2    0x54420005  /* describes where the compressed ramdisk image lives (physical address) */
#define ATAG_SERIAL     0x54410006  /* board serial number. "64 bits should be enough for everybody" */
#define ATAG_REVISION   0x54410007  /* board revision */
#define ATAG_VIDEOLFB   0x54410008  /* initial values for vesafb-type framebuffers. */
#define ATAG_CMDLINE    0x54410009  /* command line: \0 terminated string */
#define ATAG_ACORN      0x41000101  /* acorn RiscPC specific information */
#define ATAG_MEMCLK     0x41000402  /* footbridge memory clock */

struct tag_header
{
    rt_uint32_t size;
    rt_uint32_t tag;
};

struct tag_core
{
    rt_uint32_t flags;  /* bit 0 = read-only */
    rt_uint32_t pagesize;
    rt_uint32_t rootdev;
};

struct tag_mem32
{
    rt_uint32_t size;
    rt_uint32_t start;  /* physical start address */
};

struct tag_videotext
{
    rt_uint8_t  x;
    rt_uint8_t  y;
    rt_uint16_t video_page;
    rt_uint8_t  video_mode;
    rt_uint8_t  video_cols;
    rt_uint16_t video_ega_bx;
    rt_uint8_t  video_lines;
    rt_uint8_t  video_isvga;
    rt_uint16_t video_points;
};

struct tag_ramdisk
{
    rt_uint32_t flags;  /* bit 0 = load, bit 1 = prompt */
    rt_uint32_t size;   /* decompressed ramdisk size in _kilo_ bytes */
    rt_uint32_t start;  /* starting block of floppy-based RAM disk image */
};

struct tag_initrd
{
    rt_uint32_t start;  /* physical start address */
    rt_uint32_t size;   /* size of compressed ramdisk image in bytes */
};

struct tag_serialnr
{
    rt_uint32_t low;
    rt_uint32_t high;
};

struct tag_revision
{
    rt_uint32_t rev;
};

struct tag_videolfb
{
    rt_uint16_t lfb_width;
    rt_uint16_t lfb_height;
    rt_uint16_t lfb_depth;
    rt_uint16_t lfb_linelength;
    rt_uint32_t lfb_base;
    rt_uint32_t lfb_size;
    rt_uint8_t  red_size;
    rt_uint8_t  red_pos;
    rt_uint8_t  green_size;
    rt_uint8_t  green_pos;
    rt_uint8_t  blue_size;
    rt_uint8_t  blue_pos;
    rt_uint8_t  rsvd_size;
    rt_uint8_t  rsvd_pos;
};

struct tag_cmdline
{
    char cmdline[1]; /* this is the minimum size */
};

struct tag_acorn
{
    rt_uint32_t memc_control_reg;
    rt_uint32_t vram_pages;
    rt_uint8_t  sounddefault;
    rt_uint8_t  adfsdrives;
};

struct tag_memclk
{
    rt_uint32_t fmemclk;
};

struct tag
{
    struct tag_header hdr;
    union
    {
        struct tag_core         core;
        struct tag_mem32        mem;
        struct tag_videotext    videotext;
        struct tag_ramdisk      ramdisk;
        struct tag_initrd       initrd;
        struct tag_serialnr     serialnr;
        struct tag_revision     revision;
        struct tag_videolfb     videolfb;
        struct tag_cmdline      cmdline;
        struct tag_acorn        acorn;  /* acorn specific */
        struct tag_memclk       memclk; /* dc21285 specific */
    } u;
};

struct tagtable
{
    rt_uint32_t tag;
    int (*parse)(const struct tag *);
};

#define tag_member_present(tag,member)  ((unsigned long)(&((struct tag *)0L)->member + 1) <= (tag)->hdr.size * 4)
#define tag_next(t)                     ((struct tag *)((rt_uint32_t *)(t) + (t)->hdr.size))
#define tag_size(type)                  ((sizeof(struct tag_header) + sizeof(struct type)) >> 2)
#define for_each_tag(t,base)            for (t = base; t->hdr.size; t = tag_next(t))

#endif /* __RT_BOOT_ATAG_H__ */
