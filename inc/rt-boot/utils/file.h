/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#ifndef __RT_BOOT_UTILS_FILE_H__
#define __RT_BOOT_UTILS_FILE_H__

#include <rtdef.h>

#include <unistd.h>
#include <fcntl.h>

#ifndef SEEK_SET
#define SEEK_SET 0
#endif
#ifndef SEEK_CUR
#define SEEK_CUR 1
#endif
#ifndef SEEK_END
#define SEEK_END 2
#endif

enum file_status
{
    FILE_LOAD_OK = 0,
    FILE_NAME_EMPTY,
    FILE_INVALID,
    FILE_NOT_FOUND
};

enum file_status file_load_to_memory(void *load_addr, char *filename, rt_size_t *size, int (*file_check_cb)(int fd));

#endif /* _RT_BOOL_UTILS_FILE_H__ */
