/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#ifndef __RT_BOOT_UTILS_GZIP_H__
#define __RT_BOOT_UTILS_GZIP_H__

#include <rtdef.h>

#ifndef ARCH_CPU_64BIT
typedef signed int ssize_t;
#else
typedef signed long ssize_t;
#endif

/* https://tools.ietf.org/html/rfc1951 */

typedef struct
{
    rt_uint16_t magic;
    rt_uint16_t cm;
    rt_uint16_t flg;
    rt_uint32_t mtime;
    rt_uint16_t xfl;
    rt_uint16_t os;
} gzip_header_t;

typedef struct
{
    rt_uint16_t xlen;
    char        *fname;
    char        *fcomment;
    rt_uint16_t crc16;
} gzip_extra_header_t;

typedef struct
{
    rt_uint32_t crc32;
    rt_uint32_t isize;
} gzip_footer_t;

typedef struct
{
    gzip_header_t       header;
    gzip_extra_header_t extra_header;
    ssize_t             block_offset;
    gzip_footer_t       footer;
} gzip_metadata_t;

unsigned long gzip_crc32(unsigned char *buf, int len);
int gzip_get_metadata(rt_uint8_t *buf, ssize_t size, gzip_metadata_t *metadata);
void gzip_abandon_metadata(gzip_metadata_t *metadata);
void gzip_inflate(rt_uint8_t *buf, rt_uint8_t *output);

#endif /* __RT_BOOT_UTILS_GZIP_H__ */
