/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#ifndef __RT_BOOT_CMD_H__
#define __RT_BOOT_CMD_H__

#include <rtthread.h>

#define RT_BOOT_CMD_ARGS_MAX    16
#ifdef ARCH_CPU_64BIT
#define RT_BOOT_CMD_MAGIC       0x544f4f422d5452    /* RT-BOOT */
#else
#define RT_BOOT_CMD_MAGIC       0x54425452          /* RTBT */
#endif

struct rt_boot_cmd
{
    rt_ubase_t  magic;
    const char  *name;
    const char  *info;
    void        (*handler)(char**argv);
} __attribute__((packed));

#define _LINE_VAR(a)        __LINE_LINK(a, __LINE__)
#define __LINE_LINK(a, b)   __MACRO_LINK(a, b)
#define __MACRO_LINK(a, b)  a##b

#define RT_BOOT_CMD_DEFINE(x, ...)          \
static const struct rt_boot_cmd x           \
RT_USED RT_SECTION(".rti_fn.rt_boot_cmd") = \
{                                           \
    RT_BOOT_CMD_MAGIC, ##__VA_ARGS__        \
};

#define RT_BOOT_CMD(name, info, argv)               \
static void _LINE_VAR(rt_boot_cmd_handler_)(argv);  \
RT_BOOT_CMD_DEFINE(_LINE_VAR(rt_boot_cmd_),         \
#name, info, _LINE_VAR(rt_boot_cmd_handler_))       \
static void _LINE_VAR(rt_boot_cmd_handler_)(argv)

extern const char *rt_boot_auto_cmds;

int rt_boot_cmd_help(const char *info);
int rt_boot_cmd_exec(char *command, char **argv);

#endif /* __RT_BOOT_CMD_H__ */
