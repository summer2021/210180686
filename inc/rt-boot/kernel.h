/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#ifndef __RT_BOOT_KERNEL_H__
#define __RT_BOOT_KERNEL_H__

enum
{
    KERNEL_TYPE_RT_SMART,
    KERNEL_TYPE_LINUX,
    KERNEL_TYPE_UNKNOW,

    KERNEL_TYPE_EMPTY,
};

void kernel_put_no_load(void);
void kernel_put_jump_start(void);
void kernel_set_type(int type);

#endif /* __RT_BOOT_KERNEL_H__ */
