/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-15     GuEe-GUI     first version
 */

#ifndef __RT_BOOT_MACROS_H_
#define __RT_BOOT_MACROS_H_

#include <rthw.h>

#define KB 1024
#define MB (1024 * KB)

#define RT_BOOT_CPU_SYNC_IPI    (RT_STOP_IPI + 1)
#define RT_BOOT_MAX_ADDR        (~((rt_ubase_t)0UL))

#endif /* __RT_BOOT_MACROS_H_ */
